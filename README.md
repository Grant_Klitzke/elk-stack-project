# Elk-Stack-Project

Class Project-1, Elk Stack Deployment Automated ELK Stack Deployment

The files in this repository were used to configure the network depicted below.

[Network Diagram](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Diagrams/Network_Diagram.png)

These files have been tested and used to generate a live ELK deployment on Azure. They can be used to either recreate the entire deployment which is linked above. That being said, select files may be used to install only certain pieces of the diagram, such as Filebeat.

- [Install-elk.yml](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Ansible/Install-elk.yml.txt)
- [Filebeat-config.yml](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Linux/Filebeat-config.yml.txt)
- [Filebeat-playbook.yml](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Ansible/Filebeat-playbook.yml.txt)
- [Metricbeat-config.yml](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Linux/metricbeat-config.yml.txt)
- [Metricbeat-playbook.yml](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Ansible/Metricbeat-playbook.yml.txt)

This document contains the following details:
- Description of the Topology
- Access Policies
- ELK Configuration
   - Beats in Use   
   - Machines Being Monitored 
- How to Use the Ansible Build

**Description of the Topology**

The main purpose of this network is to expose a load-balanced and monitored instance of DVWA.

- What does the load balancer protect you may ask, a Load balancers protects the system from DDoS attacks by shifting attack traffic. Load Balancing also contributes to the Availability aspect of security in regards to the CIA Triad.

- Next you may be wonder what is the advantage of a jump box? Well the advantage of a jump box is to give access to the user from a single node that can be secured and monitored. Another avantage is the orgination point for launching Administrative Tasks. This sets the JumpBox as a Secure Admin Workstation. All Administrators when conducting any Administrative Task will be required to connect to the JumpBox before performing any task needed.

The configuration details of each machine may be found below.

| Name | Function | IP Address | Operating System |
| ------ | ------ | ------ | ------ |
| JumpBox | Gateway | 10.0.0.4 | Linux(ubtntu 18.04) |
| Web-1 | Server | 10.0.0.5 | Linux(ubtntu 18.04) |
| Web-2 | Server | 10.0.0.6 | Linux(ubtntu 18.04) |
| Web-3 | Server | 10.0.0.7 | Linux(ubtntu 18.04) |
| ElkServer | Elk Server | 10.1.0.4 | Linux(ubtntu 18.04) |

**Access Policies**

The machines on the internal network are not exposed to the public Internet.

The machines on the internal network are not exposed to the public Internet.

Only the Jump Box machine can accept connections from the Internet. The machine only allows access from the following IP addresses: 184.98.53.103. With the machines in the network only being able to be accessed by SSHing from Jump Box.

A summary of the access policies in place can be found in the table below.

| Name | Publicly Accessible | Allowed IP Addresses |
| ------ | ------ | ----- |
| JumpBox | no | 184.98.53.103 |
| Web-1 | no | 10.0.0.5 |
| Web-2 | no | 10.0.0.6 |
| Web-3 | no | 10.0.0.7 |
| ElkServer | no | 10.1.0.4 |

**Elk Configuration**

Ansible was used to automate configuration of the ELK machine. No configuration was performed manually, this allows you to deploy to multiple servers using a single playbook


The playbook implements the following tasks:
    
- Install
    - docker.io
    - Python-pip
    - docker container
- Launch docker container: elk
- Command: sysctl -w vm.max_map_count=262144

The following [Docker Diagram](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Diagrams/Docker_ps.png) displays the result of running `docker ps` after successfully configuring the ELK instance.

**Target Machines & Beats**

This ELK server is configured to monitor the following machines: Web-1(10.0.0.5) and Web-2(10.0.0.6)and Web-3(10.0.0.7) 

We have installed the following Beats on these machines:
    - Filebeat 
    - Metricbeat

The Beats allow us to collect the information from each machine:
- Filebeat monitors log files or locations you specify, collects log events, and forwards them either to Elasticsearch or Logstash for indexing.
- Metricbeat collects metrics from the operating system and from services running on the server.

**Using the Playbook-install-elk.yml**

In order to use the playbook, you will need to have an Ansible control node already configured. Assuming you have such a control node provisioned: 

SSH into the control node and follow the steps below:
 - Copy the playbook file to /etc/ansible.
 - Update the the configuration file to include the webservers and ElkVM (private Ip address.
 - Run the playbook, and navigate to ElkVM to check that the installation worked as expected. /etc/ansible/host should include:

Run the playbook, and SSH into the Elk vm, then run docker ps to check that the installation worked as expected. Playbook: install_elk.yml Location: /etc/ansible/install_elk.yml 

Navigate to "http://[your.ELK-VM.External.IP]:5601/app/kibana" to confirm ELK and kibana are running. You may need to try from multiple web browsers

Click 'Explore On Your Own' and you should see the following: [Kibana Dashboard](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Diagrams/Kibana_Dashboard.png)

1. Using the Playbook filebeat-playbook.yml

2. Copy the filebeat.yml file to the /etc/ansible/files/ directory

3. Update the configuration file to include the Private IP of the Elk-Server to the ElasticSearch and Kibana sections of the configuration file.

4. Create a new playbook in the /etc/ansible/roles/ directory that will install, drop in the updated configuration file, enable and configure system module, run the filebeat setup, and start the filebeat service.

5. Create a new playbook in the /etc/ansible/roles/ directory that will install, drop in the updated configuration file, enable and configure system module, run the metricbeat setup, and start the metricbeat service.

6. Run the playbooks, and navigate back to the installation page on the ELk-Server GUI, click the check data on the Module Status

7. Click the verfiy incoming Data to check and see the receiving logs from the DVWA machines.

After all of that you should see the following: [Syslog Dashboard](https://gitlab.com/Grant_Klitzke/elk-stack-project/-/blob/main/Diagrams/Filebeat_syslog.png)

Commands needed to run the configuration for the Ansible for your Elk-Server are:
```
ssh azadmin@JumpBox(Public IP)

sudo docker container list -a (locate your ansible container)

sudo docker start container (name of the container)

sudo docker attach container (name of the container)
```

** You will need to ensure all files are properly placed before running the ansible-playbooks.

